

local server = require "server"

local settings = require "settings"


-- Love callbacks ----------------
function love.load(arg)
  server:initialize(settings.ip, settings.port)
  server:registerModule("Chat")
end


function love.update(dt)
  server.sockServer:update()
end


function love.draw()
  love.graphics.clear(0, 0, 0.5)
  love.graphics.print("Server, " .. tostring(#server.sockServer:getClients()), 10, 10)


  server:debugDraw(10, 25, 15)
end



function love.keypressed(key)
  if key == "escape" then
    love.event.quit()

  elseif key == "f5" then
    server:disconnectAllClients()

  end
end



function love.quit()
  if server.sockServer:getClientCount() > 0 then
    server:disconnectAllClients()
    server.sockServer:update()
    love.event.quit() -- keep trying to quit
    return true
  end
end