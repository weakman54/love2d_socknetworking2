

local sock = require "sock"


local server = {}
server.modules = {}
server.connectedClients = {}


function server:initialize(ip, port)
  self.sockServer = sock.newServer(ip, port)

  
  self.sockServer:on("connect", function(data, client)
      print("Server: connect event")

      for modName, mod in pairs(server.modules) do
        if mod.connectHandler then mod:connectHandler(data, client) end
      end

      server.connectedClients[client] = {}
    end)

  self.sockServer:on("disconnect", function(data, client)
      print("Server: disconnect event")

      for modName, mod in pairs(server.modules) do
        if mod.disconnectHandler then mod:disconnectHandler(data, client) end
      end

      server.connectedClients[client] = nil
    end)

  server:registerModule("ID")
end


function server:disconnectAllClients()
  for _, client in ipairs(self.sockServer:getClients()) do
    client:disconnect()
  end
end



function server:registerHandlers(eventHandlers)
  for event, handler in pairs(eventHandlers) do
    self.sockServer:on(event, handler)
  end
end

function server:registerModule(name)
  local mod = require("module" .. name) -- NOTE: HARDCODED prefix, might want to change
  mod.server = self -- NOTE: this is to make the module aware of the server, but should probably make access more restricted if ever released...

  self:registerHandlers(mod.eventHandlers)

  self.modules[name] = mod -- NOTE/TODO: There is currently no checking done if a module with the same name is already loaded (though require will help keep that under check)
end



function server:debugDraw(ox, oy, textHeight)
  ox = ox or 0
  oy = oy or 0
  textHeight = textHeight or 20

  local counter = 0
  love.graphics.print("Connected clients (displaying associated name)", ox, oy + textHeight * counter)

  for client, clientData in pairs(self.connectedClients) do
    counter = counter + 1
    local name = clientData.associatedID and clientData.associatedID.name

    love.graphics.print(name or "none", ox, oy + textHeight * counter)
  end

  self.modules["ID"]:debugDraw(ox + 400, oy, textHeight) -- NOTE: Somewhat HACKy, but ID as a module was more of an experiment, and should have first-class privileges atm anyway
end



return server



