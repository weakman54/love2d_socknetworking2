

local moduleID = {}
moduleID._registeredIDs = {}


moduleID.eventHandlers = {
  ID_registerRequest = function(data, client)
    local accepted, message = moduleID:registerID(data.ID)
    local response = {accepted = accepted, message = message} -- TODO: rework this to use less bandwidth

    client:send("ID_registerResponse", response)
  end,

  ID_associateRequest = function(data, client)
    local accepted, message = moduleID:associateID(client, data.ID)
    local response = {accepted = accepted, message = message, ID = data.ID} -- TODO: rework this to use less bandwidth

    if accepted then
      moduleID:sendLoggedInClients()
    end

    client:send("ID_associateResponse", response)
  end,
}

--function moduleID:connectHandler(data, client)
--end

function moduleID:disconnectHandler(data, client) -- TODO: needs to be looked over with less tired mind
  self:disassociateClient(client)
  moduleID:sendLoggedInClients()
end




function moduleID:registerID(ID)
  if not ID.name or ID.name == "" then
    return false, "Register ID: Error: Malformed ID, name is not given!"

  elseif not ID.password or ID.password == "" then
    return false, "Register ID: Error: Malformed ID, password is not given!"

  elseif self._registeredIDs[ID.name] then
    return false, "Register ID: Error: Name " .. tostring(ID.name) .. " already registered!"

  else
    self._registeredIDs[ID.name] = ID
    return true, "Register ID: Registered name " .. tostring(ID.name) .. " successfully!"

  end
end

function moduleID:associateID(client, ID)
  local regID = self._registeredIDs[ID.name]
  local connClient = self.server.connectedClients[client]

  if not regID then
    return false,  "Login: Error: ID with name " .. tostring(ID.name) .. " is not registered!"

  elseif connClient.associatedID then
    return false, "Login: Error: Your client is already logged in with name: " .. tostring(connClient.associatedID.name) .. "!"

  elseif self:getClientByName(ID.name) then
    return false, "Login: Error: The name " .. tostring(ID.name) .. " is already in use by another client!"

  elseif regID.password ~= ID.password then
    return false, "Login: Error: Wrong password for ID with name  " .. tostring(ID.name) .. "!"


  elseif regID.password == ID.password then
    connClient.associatedID = ID
    return true, "Login: Logged in successfully with name: " .. tostring(ID.name) .. "!"

  else 
    return false, "Login: Unexpected error happened =/"
  end
end


function moduleID:disassociateClient(client) -- TODO: needs to be looked over with less tired mind
  self.server.connectedClients[client].associatedID = nil
end


function moduleID:getClientByName(name)
  for client, clientData in pairs(self.server.connectedClients) do
    if clientData.associatedID and clientData.associatedID.name == name then
      return client
    end
  end
end


function moduleID:getAssociatedClients(excluded)
  local ret = {}

  for client, clientData in pairs(self.server.connectedClients) do
    if client == excluded then
      -- "continue"
    elseif clientData.associatedID then
      table.insert(ret, client)
    end
  end

  return ret
end

function moduleID:getAssociatedClientNames(excluded) -- TODO: see if there is a way to use the above function to simplify this one (and reduce duplication)
  local ret = {}

  for client, clientData in pairs(self.server.connectedClients) do
    if client == excluded then
      -- "continue"
    elseif clientData.associatedID then
      table.insert(ret, clientData.associatedID.name)
    end
  end

  return ret
end


function moduleID:broadcastToLoggedIn(event, data, exclude)
  local clients = self:getAssociatedClients(exclude)
  
  for _, c in ipairs(clients) do
    c:send(event, data)
  end
end


function moduleID:sendLoggedInClients()
  local broadcast = {loggedInClients = self:getAssociatedClientNames()}
  self:broadcastToLoggedIn("ID_loggedInClients", broadcast)
end


function moduleID:debugDraw(ox, oy, textHeight)
  ox = ox or 0
  oy = oy or 0
  textHeight = textHeight or 20

  local counter = 0
  love.graphics.print("Registered IDs",  ox, oy + textHeight * counter)

  for name in pairs(self._registeredIDs) do
    counter = counter + 1
    love.graphics.print(name, ox, oy + textHeight * counter)
  end
end



return moduleID




