

local chat = {}

chat.eventHandlers = {
  Chat_messageSend = function(data, client)

    if type(data.ID) ~= "table" then
      print("Chat: Warning: Malformed message request, given info: \n\tdata.ID: " .. tostring(data.ID) )
      return
    end

    local name, message = data.ID and data.ID.name, data.message    

    if type(name) ~= "string" or type(message) ~= "string" then 
      print("Chat: Warning: Malformed message request, given info: \n\tname: " .. tostring(name) .. "\n\tmessage: " .. tostring(message)) 
      return 
    end -- TODO: make error handling for this stuff

    print("chat event ", name, message)


    local broadcast = {name = name, message = message}
    chat.server.modules["ID"]:broadcastToLoggedIn("Chat_messageReceive", broadcast)
  end,
}




return chat