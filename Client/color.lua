
return {
  white   = {1, 1, 1},
  black   = {0, 0, 0},
  
  red     = {1, 0, 0},
  yellow  = {1, 1, 0},
  green   = {0, 1, 0},
  cyan    = {0, 1, 1},
  blue    = {0, 0, 1},
  magenta = {1, 0, 1},
}