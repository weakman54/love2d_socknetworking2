

local nk = require "nuklear"

local statusFeed = require "statusFeed"
local color = require "color"

local W, H = love.graphics.getDimensions() -- NOTE/TODO: this does not work with scaling probably


local chat = {}
chat.eventHandlers = {
  Chat_messageReceive = function(data)
    table.insert(chat.messages, {data.name, data.message})
  end,
}



chat.messages = {}



chat.messageField = {value = "", state = "inactive"}

function chat:GUI()
  if nk.windowBegin("Chat", W/2 - 300/2 - 300, H/2 - 180/2, 300, 180, "border", "title", "movable", "minimizable", "scalable") then

    local x, y, width, height = nk.windowGetContentRegion()


    nk.layoutSpaceBegin('dynamic', height - 40, 1)

    nk.layoutSpacePush(0, 0, 1, 1)

    local _, _, widgetWidth = nk.widgetBounds()
    local chatRatios = {0.2, 0.05, 0.75}
    local nameWidth = widgetWidth * chatRatios[1]
    local msgWidth = widgetWidth * chatRatios[#chatRatios]

    local font = love.graphics.getFont()
    local fontHeight = font:getHeight()
    local padding = 5

    if nk.groupBegin("MessagesWindow", "scrollbar") then        
      for _, chatData in ipairs(self.messages) do
        local nameNeededHeight = math.ceil(font:getWidth(chatData[1]) / nameWidth) * (fontHeight + padding)
        local msgNeededHeight = math.ceil(font:getWidth(chatData[2]) / msgWidth) * (fontHeight + padding)
        local neededHeight = math.max(nameNeededHeight, msgNeededHeight)

        nk.layoutRow("dynamic", neededHeight, chatRatios)
        nk.label(chatData[1] .. ":", "wrap")
        nk.spacing(1)
        nk.label(chatData[2], "wrap")
      end
      nk.groupEnd()
    end

    nk.layoutSpaceEnd()


    nk.layoutRow('dynamic', 30, {0.8, 0.2})

    local state, changed = nk.edit("field", self.messageField)
    self.messageField.state = state
    if state == "active" or state == "inactive" then
    else
      print(state, changed) 
    end

    if nk.button("Send") then
--      self:tryRegisterID({name = self.nameField.value, password = self.passField.value})
    end

  end
  nk.windowEnd()
  --
end


function chat:sendMessage()
  local ID = self.client.associatedID

  if not ID then
    statusFeed:push("Chat: Error: can't send message when logged out!", color.red)
    return
  end


  local message = self.messageField.value
  -- TODO: check message is valid proper
  if not message or message == "" then
    statusFeed:push("Chat: Error: Can't send empty message!", color.red)
    return
  end


  local data = {ID = ID, message = message}
  self.client.sockClient:send("Chat_messageSend", data)
  self.messageField.value = ""
end

function chat:update(dt)
  self:GUI()
end 


function chat:keypressed(key)
  if key == "return" and self.messageField.state == "active" then
    self:sendMessage()
  end
end




return chat