

do -- setColor HACK Because building nuklear is a fucking pain...
  setColor = love.graphics.setColor -- Needs to be global for stuff to be able to use it outside of main... 
-- TODO: try to find a better way to fix this
-- Should work generically now for most situations, only problem happens for old system when using _all_ values <= 1, which should be rare
  function love.graphics.setColor(r,g,b,a)
--    local ri, rf = math.modf(r)
--    local gi, gf = math.modf(g)
--    local bi, bf = math.modf(b)

--    local ai, af
--    if a then
--      local ai, af = math.modf(a)
--    end


--    local greaterThan1 = r > 1 or g > 1 or b > 1 or (a and a > 1) -- definitely old system
--    local hasFractional = rf ~= 0 or gf ~= 0 or bf ~= 0 or (a and af ~= 0)

--    if greaterThan1 then 
      a = a or 255
      setColor(r/255, g/255, b/255, a/255)

--    else -- Probably new system
--      a = a or 1
--      setColor(r, g, b, a)
--    end
  end
end
--


local nk = require "nuklear"
local sock = require "sock"


local statusFeed = require "statusFeed"

local color  = require "color"

local settings = require "settings"
local client = require "client"



function love.load(arg)
  if arg[#arg] == "-debug" then require("mobdebug").start() end

  nk.init()

  client:initialize(settings.ip, settings.port)
  client:registerModule("Chat")
end


function love.update(dt)
  nk.frameBegin()


  client:update(dt)
  statusFeed:update(dt)


  nk.frameEnd()
end



-- Draw stuff ----------------------------
do -- Indicators vvvv
  function drawIndicator(x, y, text, indicatorColor)
    x = x or 0
    y = y or 0

    setColor(indicatorColor)

    love.graphics.circle("fill"     , x, y, 10)
    love.graphics.print(text, x + 15, y - 8)

    setColor(color.white)
  end
--

  function drawConnectedIndicator(x, y) 
    local indicatorColor = color.red

    if client.sockClient:isConnected() then
      indicatorColor = color.green

    elseif client.sockClient:isConnecting() then
      indicatorColor = color.yellow

    end

    drawIndicator(x, y, client.sockClient:getState(), indicatorColor)
  end
--

  function drawLoggedinIndicator(x, y)
    if client.associatedID then
      drawIndicator(x, y, "Logged In as: " .. tostring(client.associatedID.name), color.green)

    else
      drawIndicator(x, y, "Not logged in", color.red)

    end
  end
end
-- Indicators ^^^^

function love.draw()
  nk.draw() -- NOTE: think about when this is drawn

  setColor(color.white)
  love.graphics.print("Client", 10, 7)

  drawConnectedIndicator(20, 35)
  drawLoggedinIndicator(20, 57)

  statusFeed:draw()
end
--



-- Other Love callbacks -------------------
function love.keypressed(key, scancode, isrepeat)
--  if nk.keypressed(key, scancode, isrepeat) then return end -- event consumed
  client:keypressed(key, scancode, isrepeat) -- NOTE: this may lead to conflicts because bs, but bleh


  if nk.keypressed(key, scancode, isrepeat) then 
    return 
  end -- event consumed

  if key == "escape" then
    love.event.quit()

  elseif key == "f5" then
    client:tryConnect()

  end
end


function love.quit()
  client:quit()
end




-- Currently only nuklear callback usage --------
function love.keyreleased(key, scancode)
  if nk.keyreleased(key, scancode) then return end -- event consumed
end

function love.mousepressed(x, y, button, istouch)
  if nk.mousepressed(x, y, button, istouch) then return end -- event consumed
end

function love.mousereleased(x, y, button, istouch)
  if nk.mousereleased(x, y, button, istouch) then return end -- event consumed
end

function love.mousemoved(x, y, dx, dy, istouch)
  if nk.mousemoved(x, y, dx, dy, istouch) then return end -- event consumed
end

function love.textinput(text)
  if nk.textinput(text) then return end -- event consumed
end

function love.wheelmoved(x, y)
  if nk.wheelmoved(x, y) then return end -- event consumed
end












