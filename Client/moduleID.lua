
local nk = require "nuklear"



local W, H = love.graphics.getDimensions() -- NOTE/TODO: this does not work with scaling probably



local moduleID = {}
moduleID.loggedInClients = {}


moduleID.eventHandlers = {
  ID_registerResponse = function(data)
    moduleID.client.registerResponse = data -- HACKish, didn't think this through too far, but it works so eh...
  end,

  ID_associateResponse = function(data)
    if data.accepted then
      moduleID.client.associatedID = data.ID
    end

    moduleID.client.associateResponse = data -- HACKish, didn't think this through too far, but it works so eh... 
  end,

  ID_loggedInClients = function(data)
    print("loggedInClients Event: ", data.loggedInClients)
    moduleID.loggedInClients = data.loggedInClients -- TODO: error checking
  end,

}




function moduleID:connectHandler(data)
  self:disassociateID()
end

function moduleID:disconnectHandler(data)
  self:disassociateID()
end



-- TODO: separate into GUI field?
moduleID.nameField = {value = "", state = "inactive"}
moduleID.passField = {value = "", state = "inactive"}

function moduleID:GUILogin()
  if nk.windowBegin("Login", W/2 - 300/2, H/2 - 180/2, 300, 180, "border", "title", "movable", "minimizable", "scalable") then
    -- Name Field
    nk.layoutRow('dynamic', 30, {0.2, 0.8})

    nk.label("Name: ")

    self.nameField.state = nk.edit("field", self.nameField)



    -- Password Field
    nk.layoutRow('dynamic', 30, {0.2, 0.8})

    nk.label('Password:')

    self.passField.state = nk.edit("field", self.passField)



    -- Register button
    nk.layoutRow("dynamic", 30, 1)
    if nk.button("Register") then
      self:tryRegisterID({name = self.nameField.value, password = self.passField.value})
    end


    -- Log in button
    nk.layoutRow("dynamic", 30, 1)
    if nk.button("Log in") then
      self:tryLogin({name = self.nameField.value, password = self.passField.value})
    end
  end

  nk.windowEnd()
end


function moduleID:GUILoggedInList()
  -- NOTE/TODO: test changing this name to something more useful later, a lot of values seem to crash nuklear, and I think it has to do with some kind of caching
  --            so waiting a while might help?
  if nk.windowBegin("ThisNameHasNotBeenUsed", "Connected Clients: ", W/2 - 300/2, H/2 - 180/2, 300, 180, "border", "title", "movable", "minimizable", "scalable", "scrollbar") then
    if self.loggedInClients then
      local x, y, width, height = nk.windowGetContentRegion()

      local font = love.graphics.getFont()
      local fontHeight = font:getHeight()
      local padding = 5


      for _, clientName in ipairs(self.loggedInClients) do
        local nameNeededHeight = math.ceil(font:getWidth(clientName) / width) * (fontHeight + padding)

        nk.layoutRow("dynamic", nameNeededHeight, {1})
        nk.label(clientName, "wrap")

      end
    end
  end
  nk.windowEnd()
end



function moduleID:update()
  -- GUI --------
  if not self.client.associatedID then
    self:GUILogin()
  else
    self:GUILoggedInList()
  end

  self.client:pushResponses({"associateResponse", "registerResponse"})
end



-- NOTE: Register and Login also use response message callbacks above, but I separated them since I might want to refactor that (not sure yet tho)
-- Register
function moduleID:tryRegisterID(ID)
  if self.client.sockClient:isDisconnected() then
    self.client:tryConnect()
  end
  self.client.sockClient:send("ID_registerRequest", {ID = ID})
end


-- Login
function moduleID:tryLogin(ID)
  if self.client.sockClient:isDisconnected() then
    self.client:tryConnect()
  end
  self.client.sockClient:send("ID_associateRequest", {ID = ID})
end



function moduleID:disassociateID()
  self.client.associatedID = nil -- TODO: make sure this function does all it needs to do
end



function moduleID:keypressed(key)
  if key == "return" and nk.windowIsActive("Login") then
    self:tryLogin({name = self.nameField.value, password = self.passField.value})
  end
end



return moduleID



