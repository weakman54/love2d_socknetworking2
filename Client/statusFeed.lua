

local W, H = love.graphics.getDimensions() -- NOTE/TODO: this does not work with scaling probably


-- status feed at bottom of screen
local statusFeed = {}


statusFeed.fadeRate = 0.1 -- alpha/second
statusFeed._items = {} 


function statusFeed:push(text, color)
  table.insert(self._items, 1, {text = text, color = color, alpha = 1})
end

function statusFeed:update(dt)
  for _, item in ipairs(self._items) do 
    item.alpha = item.alpha - self.fadeRate * dt

    if item.alpha <= 0 then
      table.remove(self._items) -- ASSUMPTION: the item with lowest alpha will be at end of list
    end
  end
end

function statusFeed:draw()
  for i, item in ipairs(self._items) do
    setColor(item.color[1], item.color[2], item.color[3], item.alpha) -- NOTE: uses modified setColor because nuklear color hack
    love.graphics.print(item.text, 20, H - 30 * i)
  end
end


return statusFeed