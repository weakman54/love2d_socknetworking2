
local sock = require "sock"


local color = require "color" -- only used by statusFeed
local statusFeed = require "statusFeed" -- NOTE/TODO: This is only used by pushResponses, might want to factor this differently


local client = {}
client.modules = {}

client.defaultHandlers = {
  connect = function(data) 
    for modName, mod in pairs(client.modules) do
      if mod.connectHandler then mod:connectHandler(data) end
    end
  end,

  disconnect = function(data) 
    for modName, mod in pairs(client.modules) do
      if mod.disconnectHandler then mod:disconnectHandler(data) end
    end
  end,
}


function client:initialize(ip, port) -- TODO: errorhandling
  assert(ip and port, "client:new(): must supply ip and port!")


  self.sockClient = sock.newClient(ip, port)
  self.sockClient:connect()    -- HACK: client doesn't actually get properly initialized until it has tried to connect once..
  self.sockClient:disconnect() --       This shouldn't affect anything, but yeah...


  self:registerHandlers(self.defaultHandlers) 

  client:registerModule("ID") -- Privileged module atm, since eveything else will probably rely on it
end


function client:update(dt)
  if not self.sockClient:isDisconnected() then
    self.sockClient:update()
  end

  if not self.associatedID then
    self.modules.ID:update(dt)
  else
    for modName, mod in pairs(self.modules) do
      mod:update(dt)
    end
  end
end

function client:tryConnect()
  self.sockClient:disconnect() -- Feels like a HACK, but works, so eh...
  self.sockClient:connect()
  self.sockClient:update() -- NOTE: client:update calls module updates, Make sure not to end up in an infinite recursion by calling client:update instead of client.sockClient:update here!
end


function client:quit()
  self.sockClient:disconnect()
  self.sockClient:update()
end


function client:registerHandlers(eventHandlers)
  if not eventHandlers then error("client:registerHandlers(): must supply eventHandlers as table!", 2) end

  for event, handler in pairs(eventHandlers) do
    self.sockClient:on(event, handler)
  end
end

function client:registerModule(name)
  local mod = require("module" .. name) -- NOTE: HARDCODED prefix, might want to change
  mod.client = self -- NOTE: this is to make the module aware of the client, but should probably make access more restricted if ever released...

  if not mod.eventHandlers then error("client:registerModule(): can't register module '" .. tostring(name or "nil") .. "'! it does not have an eventHandlers table!", 2) end
  self:registerHandlers(mod.eventHandlers)

  self.modules[name] = mod -- NOTE/TODO: There is currently no checking done if a module with the same name is already loaded (though require will help keep that under check)
end


function client:consumeResponse(responseKey)
  local response = self[responseKey]
  if not response then return end

  local accepted, message = response.accepted, response.message
  self[responseKey] = nil

  return accepted, message
end

function client:pushResponses(respKeys)
  for _, respKey in ipairs(respKeys) do
    local accepted, message = self:consumeResponse(respKey)
    if accepted ~= nil then
      statusFeed:push(message, accepted and color.green or color.red)
    end      
  end
end





function client:keypressed(key, scancode, isrepeat)
  for modName, mod in pairs(self.modules) do
    if mod.keypressed then mod:keypressed(key, scancode, isrepeat) end
  end
end


return client




